import { createStore } from 'redux';
import worker from '../reducers/index';
const store = createStore(worker);
export default store;