import * as firebase from "firebase";

import store from './store/index';
import { addData } from './actions/index';

const FirebaseConfig = {
    apiKey: "AIzaSyDyGh5n97Mbn_6r5j5TUZwB1m-75r8E7WI",
    authDomain: "downloadinnovation2018.firebaseapp.com",
    databaseURL: "https://downloadinnovation2018.firebaseio.com",
    projectId: "downloadinnovation2018",
    storageBucket: "downloadinnovation2018.appspot.com",
    messagingSenderId: "868924858016"
}

firebase.initializeApp(FirebaseConfig);

var db = firebase.database();

export const anonymSignIn = () => {
    firebase.auth().signInAnonymously().catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
    });

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            store.dispatch(addData({uid: user.uid}))
        } else {
            store.dispatch(addData({uid: null}))
        }
    });
}

export const writeData = () => {
    const { nome, cognome, email, classificazione, commento, uid } = store.getState().callData;
    const data = new Date();
    var formattedDate = data.getFullYear()+'-'+data.getMonth()+1+'-'+data.getDate()+'T'+data.getHours()+':'+data.getMinutes()+':'+data.getSeconds();

    db.ref('utenti/' + uid +'/segnalazione').set({
        luogo: 'luogo',
        nome: nome,
        cognome: cognome,
        email: email,
        classificazione: classificazione,
        data: formattedDate,
        commenti: commento,
        user: uid,
        tag: 'piante',
    }, function(error) {
        if (error) {
            console.log(error)        
        } else {
          // Data saved successfully!
        }
    });
}