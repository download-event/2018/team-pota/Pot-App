import { ADD_DATA } from '../actions/index';

const initialState = {
  key: null,
  callData: {},
}

function worker(state = initialState, action) {
  state['callData'];
  switch(action.type) {
    case ADD_DATA:
      return Object.assign({}, state, {
        callData: {
          ...state.callData,
          ...action.data
        }
      });
    default:
      return state;
  }
}

export default worker;