import React, { Component } from 'react';

import Login from './components/Login';
import Navigation from './components/Navigation';

export default class Home extends Component {

render() {
    return (
        <React.Fragment>
            <Navigation />
            <div style={styles.container}>
                <React.Fragment>
                    <div style={styles.topLabel}>
                        Invia una segnalazione
                    </div>
                    <div style={styles.separator}></div>
                    <div style={styles.bottomLabel}>
                    Accedi per iniziare
                    </div>
                </React.Fragment>
                <div style={styles.loginContainer}>
                    <Login/>  
                </div>
            </div>
        </React.Fragment>
    );
    }
}

const styles = {
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: 'auto'
    },
    separator: {
        height: 1,
        background: '#717171',
        width: 200,
        margin: 'auto',
        marginTop: 10,
        marginBottom: 10,
    },
    topLabel: {
      fontSize: 40,
      textAlign: 'center',
    },
    bottomLabel: {
      fontSize: 20,
      textAlign: 'center',
    },
    loginContainer: {
      margin: 'auto'
    }
  }