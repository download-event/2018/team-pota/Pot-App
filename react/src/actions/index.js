export const ADD_DATA = 'ADD_DATA';


/**
 * 
 * @param {object} data 
 */

export function addData(data) {
    return { type: ADD_DATA, data}
}